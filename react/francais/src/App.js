import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from "./components/navbar/navbar";
import Logo from "./components/logo/logo";
import ContentField from "./components/contentField/contentField";
import FooterField from "./components/footer/footer";

class App extends Component {
  render() {
    return (
        <div className="App">
          <header className="App-header">
            <Navbar/>
              <Logo/>
          </header>
            <ContentField/>
            <FooterField/>
        </div>
    );
  }
}

export default App;
