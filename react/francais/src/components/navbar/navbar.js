import React, { Component } from 'react';
import logo from "../../logo.svg";

class Navbar extends Component {
    render() {
        return (
            <div className="Navbar">
                Apprendre le francais avec moi
            </div>
        );
    }
}

export default Navbar;
